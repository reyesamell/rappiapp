//
//  MainSolidButton.swift

//
//  Created by Hugo Reyes on 2/7/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//
import Foundation
import UIKit

@IBDesignable

class MainSolidButton: UIButton {
    
    init(text: String, color: UIColor = UIColor.gray){
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))

        
        self.tintColor = UIColor.black
        self.titleLabel?.text = text
        self.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        self.backgroundColor = color
        self.translatesAutoresizingMaskIntoConstraints = false
        self.isEnabled = true
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
