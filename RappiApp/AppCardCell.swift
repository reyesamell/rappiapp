//
//  AppCardCell.swift
//  RappiApp
//
//  Created by iMac3 on 2/9/17.
//  Copyright © 2017 iMac3. All rights reserved.
//

import UIKit

class AppCardCell: UICollectionViewCell {
    
    var json: NSDictionary?

    let imageApplication = CustomImageClass(image: #imageLiteral(resourceName: "connections-black"))
    
    let appLabel = LabelMedium(text: "", size: 15)
    
    let button = MainSolidButton(text: "", color: .white)

    
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.white
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 5
        
        self.imageApplication.layer.cornerRadius = 8
        self.imageApplication.clipsToBounds = true
        self.imageApplication.layer.shadowColor = UIColor.gray.cgColor
        self.imageApplication.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.imageApplication.layer.shadowOpacity = 1
        self.imageApplication.layer.shadowRadius = 5
        
        self.appLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.appLabel.numberOfLines = 0
        self.button.layer.cornerRadius = 20
        self.addSubviews()
        //self.setupLayout()
        
    }
    
    func addSubviews(){
    
        self.addSubview(self.button)
        self.button.addSubview(self.appLabel)
        self.button.addSubview(self.imageApplication)
 
    
    }
    
    func setupLayout(){
 
        self.layoutIfNeeded()
        

        self.button.buildingAnchorConstraint(
            referenceView: self,
            topAnchorConstant: 0,
            heightAnchorMultiplier: 1,
            widthAnchorMultiplier: 1,
            centerXAnchor: true)

        
        self.button.layer.cornerRadius = 20
        self.button.layoutIfNeeded()
        
        
        let pointEstButton = RelativeLayoutUtilityClass(referenceFrameSize: self.button.bounds.size)
        
        self.imageApplication.buildingAnchorConstraint(
            referenceView: self.button,
            topAnchorConstant: pointEstButton.relativeHeight(multiplier: 0.150),
            centerXAnchor: true,
            fixedHeight: 75,
            fixedWidth: 75)
        
        self.appLabel.buildingAnchorConstraint(
            referenceView: self.button,
            widthAnchorMultiplier: 0.8,
            centerXAnchor: true,
            fixedHeight: 50)
        
        self.appLabel.topAnchor.constraint(equalTo: self.imageApplication.bottomAnchor, constant: 10).isActive = true
    }
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
