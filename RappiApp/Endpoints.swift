
//
//  Endpoints.swift

//
//  Created by Hugo Reyes on 1/24/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//
import Foundation
import UIKit


class Endpoints {
    
    let urlString = "https://itunes.apple.com/us/rss/topfreeapplications/limit=20/json"
    

    func requestInfoFromURL(completionHandler: @escaping (NSDictionary?, [AnyObject]?, NSError?) -> Void ) -> URLSessionTask{
    

        let url = URL(string: self.urlString)
        
        let session = URLSession.shared
//        
        let task = session.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            guard let data = data, let _ :URLResponse = response, error == nil else {
                completionHandler(nil, nil, error as NSError?)
                print("error")
                return
            }
            
       
            do{
            
                let originalJson = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            
            
                    guard let jsonFeed = originalJson?.value(forKey: "feed") as? NSDictionary
                        , let entryList = jsonFeed["entry"] as? [AnyObject]
                
                        else{
                            
                            completionHandler(nil, nil, nil)
                            return
                        }
                
                            completionHandler(jsonFeed, entryList, nil)
                
                    }
                            
                catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    
                }
            
            
        })
        
     
        task.resume()
        
        return task
    }
    
    
    func requestHttps(callback: @escaping  (NSDictionary, [AnyObject]) -> Void )
    
    
    {
//        
        // Sign in user for getting the token
        _ = self.requestInfoFromURL(completionHandler: { (jsonFeed, entryList, error) in
            
            
            if ((jsonFeed != nil) && (entryList != nil)){
                // If everything is succesfull run the callback
                callback(jsonFeed!, entryList!)
            }else{
                print(error!)
            }
            


          }) // End of else tokenString

    } // End of requestHttpToken
    
    
    func downloadImage(index: Int, urlString: String, callback: @escaping  (Int, UIImage?) -> Void){
        print("url", urlString)
        let catPictureURL = URL(string: urlString)!
        
        let session = URLSession(configuration: .default)
        
        // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
        let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
            // The download has finished.
            if let e = error {
                print("Error downloading cat picture: \(e)")
            } else {
                // No errors found.
                // It would be weird if we didn't have a response, so check for that too.
                if let res = response as? HTTPURLResponse {
                    print("Downloaded cat picture with response code \(res.statusCode)")
                    if let imageData = data {
                        // Finally convert that Data into an image and do what you wish with it.
                        let image = UIImage(data: imageData)
                        // Do something with your image.
                        callback(index, image!)
                        
                    } else {
                        print("Couldn't get image: Image is nil")
                        
                        callback(index, nil)
                    }
                } else {
                    print("Couldn't get response code for some reason")
                    callback(index, nil)
                }
            }
        }
        
        downloadPicTask.resume()
        
        
    }
    
    
    
    

    
    
    
} // End of endpoint class
