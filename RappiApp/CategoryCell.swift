//


//
//  Created by Hugo Reyes on 2/8/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    // This is the custom cell of the tableView object that deals
    // with the message information.
    let categoryView = CategoryView()
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Disable selection:
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // Calling a custom method for setting the views

        self.layoutIfNeeded()
        self.addSubviews()
        
    }
    
    // This is required for the compiler. Swift stuff.
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func addSubviews(){
        
        self.addSubview(self.categoryView)
        
    }
    
    func setupViews(){
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 5
        
        self.backgroundColor = .clear
        
        self.categoryView.buildingAnchorConstraint(
            referenceView: self,
            topAnchorConstant: 7.5,
            bottomAnchorConstant: -7.5,
            widthAnchorMultiplier: 0.5,
            centerXAnchor: true,
            centerYAnchor: true)
        
        self.categoryView.setupLayout()
        
        
    }


    
}
