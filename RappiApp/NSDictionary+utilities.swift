//
//  NSDictionary+utilities.swift
//  RappiApp
//
//  Created by iMac3 on 2/11/17.
//  Copyright © 2017 iMac3. All rights reserved.
//

import Foundation

extension NSDictionary {

    func getDictFromKey(key: String) -> NSDictionary{
        return self.value(forKey: key) as! NSDictionary
    }
    
    func getListFromKey(key: String) -> [Any]{
        return self.value(forKey: key) as! [Any]
    }
    
    
}
