//
//  RelativeLayoutUtilityClass.swift
//
//
//  Created by Hugo Reyes on 2/1/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//

import Foundation
import UIKit

class RelativeLayoutUtilityClass {
    
    var heightFrame: CGFloat?
    var widthFrame: CGFloat?
    
    init(referenceFrameSize: CGSize){
        heightFrame = referenceFrameSize.height
        widthFrame = referenceFrameSize.width
    }
    
    func relativeHeight(multiplier: CGFloat) -> CGFloat{

        return multiplier * self.heightFrame!
    }
    
    func relativeWidth(multiplier: CGFloat) -> CGFloat{
        return multiplier * self.widthFrame!

    }
    
    

}
