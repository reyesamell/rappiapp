//
//  Colors.swift
//  pizzaApp
//
//  Created by Hugo Reyes on 12/9/16.
//  Copyright © 2016 Hugo Reyes. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    // This extension creates custom colors for use in the iter application.
    // In other words, this is the color palette application.
    
    // c1 color es the one who is darker and the nearest to the color 0,0,0 in the rbg unit.
    // The subsequent colors are less dark.
    
    class var orangeRappi: UIColor {
        return UIColor.createColorObject(r: 242, g: 136, b: 88)
    }
    
    class var orangeRappiClear: UIColor {
        return UIColor.createColorObject(r: 250, g: 155, b: 97)
    }
    
    class var greenRappiClear: UIColor {
        return UIColor.createColorObject(r: 48, g: 212, b: 123)
    }
    
    class var redRappiClear: UIColor {
        return UIColor.createColorObject(r: 254, g: 97, b: 73)
    }
    
    class var yellowRappiClear: UIColor {
        return UIColor.createColorObject(r: 255, g: 213, b: 82)
    }
    
    static func createColorObject(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat = 1) -> UIColor{
        let constant : CGFloat = 255
        return UIColor(red: r / constant, green: g / constant, blue: b / constant, alpha: 1.0)
    }
    
}

