//
//  GeneralAppInfo.swift
//  RappiApp
//
//  Created by iMac3 on 2/12/17.
//  Copyright © 2017 iMac3. All rights reserved.
//

import UIKit

class AppClass: NSObject {

    var title: String = ""
    var image: UIImage = #imageLiteral(resourceName: "1px transparent")
    var appDescription: String = ""
    var urlImage: String = ""
    
    init(title: String, image: UIImage = #imageLiteral(resourceName: "1px transparent"), urlImage: String = "", appDescription: String = ""){
        
        self.title = title
        self.image = image
        self.appDescription = appDescription
        self.urlImage = urlImage
    
    }
    

}
