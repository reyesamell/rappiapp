//
//  CustomImageClass.swift

//  Created by Hugo Reyes on 1/31/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//

import UIKit

class CustomImageClass: UIImageView {
    // LogoCategoryc class
    
    init(){
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override init(image: UIImage?){

        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.image = image?.withRenderingMode(.alwaysOriginal)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
