//
//  String+Uitilities.swift

//  Created by Hugo Reyes on 1/11/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//
import UIKit

extension String {
    // This variable set the string attributes:
    
    private func returnAttributeString(font: UIFont, color: UIColor, spacing: CGFloat = 4, letterSpacing: CGFloat = 0) -> NSMutableAttributedString{
        
        let attributedString = NSMutableAttributedString(string: self, attributes: [NSFontAttributeName: font])
        
        // Adding more atrributes
        // Modifing the lineSpacing
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = CGFloat(spacing)
        attributedString.addAttributes([NSParagraphStyleAttributeName: paragraphStyle], range: NSRange(location: 0, length: attributedString.length))
        attributedString.addAttributes([NSForegroundColorAttributeName: color], range: NSRange(location: 0, length: attributedString.length))
        attributedString.addAttributes([NSKernAttributeName: letterSpacing], range: NSRange(location: 0, length: attributedString.length))
        
        return attributedString
    }
    
    func createAttributeString(font: UIFont, color: UIColor, spacing: CGFloat = 4, letterSpacing: CGFloat = 0) -> NSMutableAttributedString{
        
        let attributedString = NSMutableAttributedString(string: self, attributes: [NSFontAttributeName: font])
        
        // Adding more atrributes
        // Modifing the lineSpacing
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = CGFloat(spacing)
        attributedString.addAttributes([NSParagraphStyleAttributeName: paragraphStyle], range: NSRange(location: 0, length: attributedString.length))
        attributedString.addAttributes([NSForegroundColorAttributeName: color], range: NSRange(location: 0, length: attributedString.length))
        return attributedString
    
    }
    
    
    func calculateStringSize(font: UIFont, width: CGFloat = 215) -> CGRect{
 
        let attString = self.returnAttributeString(font: font, color: UIColor.orangeRappi)
        // Estimated frame size:
        let estimatedFrame:CGRect = attString.boundingRect(with: CGSize(width: width, height:
            CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context:nil )
        
        return estimatedFrame
    }
    
    
}
