//
//  CategoryView.swift
//
//  Created by Hugo Reyes on 2/8/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//

import UIKit


class CategoryView: UIView {


    let button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("cllick", for: .normal)
        return button
        
    }()
    
    // Global variables
    var pointEstimator: RelativeLayoutUtilityClass?
    
    // MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        addSubviews()
        //setupLayout()
        
    }
    
    required init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        addSubviews()
        //setupLayout()
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    // MARK: Build View hierarchy
    
    func addSubviews(){
        
        self.addSubview(self.button)
        
    }

    func setupLayout(){
        
        self.button.layer.cornerRadius = 30
        self.layoutIfNeeded()
        self.pointEstimator = RelativeLayoutUtilityClass(referenceFrameSize: self.frame.size)
        
        self.button.buildingAnchorConstraint(
            referenceView: self,
            widthAnchorMultiplier: 1,
            centerXAnchor: true,
            centerYAnchor: true,
            fixedHeight: 80)
        
    }
    

    
    
  
    
}
