//
//  CoreDataUtilities.swift
//
//  Created by Hugo Reyes on 1/25/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//
import UIKit
import Foundation
import CoreData

class CoreDataUtilities: NSObject {
    
    func entityIsEmpty(entity: String) -> Bool
    {
        
        // Fecth the core data to the caller.
        let context: NSManagedObjectContext = getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entity)
        var coreElements: [NSManagedObject] = [NSManagedObject]()
        
        do {
            
            coreElements = try context.fetch(fetchRequest)
            if coreElements.last == nil{
            return true
            }
            return false
        } catch _ as NSError {
            
           return true
        }


    }

    
    func saveNumberScreen(screenNumber: String){
        // This save the current screen of the application in core data
        
        let context: NSManagedObjectContext = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Screen", in: context)
        
        let managedObject = NSManagedObject(entity: entity!, insertInto: context)
        managedObject.setValue(screenNumber, forKeyPath: "number")
        
        // Now we need to "commit" the changes
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    
    func readNumberScreen() -> String{
        // This func read from core data what view was the current last time app was running
        let data = self.fetchFromCoreData(entityName: "Screen", key: "number")
        
        if ((data.last?.value(forKey: "number") as! String) == ""){
            return "1"
        }
        else{
            return (data.last?.value(forKey: "number") as! String)
        }
    }
    
    
    func saveAppList(appsList: [AppClass]){
        // This func saves all apps data into core data
        
        let context: NSManagedObjectContext = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "App", in: context)
        
        for index in 0...(appsList.count - 1){
            
            let managedObject = NSManagedObject(entity: entity!, insertInto: context)
            let dataImage = UIImagePNGRepresentation(appsList[index].image)! as Data
            
            managedObject.setValue(dataImage, forKey: "image")
            managedObject.setValue(appsList[index].title, forKeyPath: "title")
            managedObject.setValue(appsList[index].appDescription, forKey: "descriptionApp")
            
            managedObject.setValue(appsList[index].urlImage, forKey: "imageUrl")
            
            // Now we need to "commit" the changes
            do {
                try context.save()
                
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        } // End of for loop

    } // End of saveAppList
    
    
    func readAppList() -> [AppClass]{
    
        let titlesObjects = fetchFromCoreData(entityName: "App", key: "title")
        
        let imagesObjects = fetchFromCoreData(entityName: "App", key: "image")
        
        let descripObjects = fetchFromCoreData(entityName: "App", key: "descriptionApp")
        
        let urlImage = fetchFromCoreData(entityName: "App", key: "imageUrl")
        var appsList: [AppClass] = [AppClass]()
        
        for index in 0...(titlesObjects.count - 1){
            appsList.append(AppClass(title: titlesObjects[index].value(forKey: "title") as! String,
                image: UIImage(data: (imagesObjects[index].value(forKey: "image") as! Data))!,
                urlImage: urlImage[index].value(forKey: "imageUrl") as! String,
                appDescription: descripObjects[index].value(forKey: "descriptionApp") as! String))
        }
        return appsList
    }
    
    


    func readAppList() throws {
        throw MyError.RuntimeError("Error")
    }

    func saveCoreDataInfo(InfoString: [String], entityName: String, key: String){
        // Save the token in the coreData
        let context: NSManagedObjectContext = getContext()
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        
        for index in 0...(InfoString.count - 1){
            let tokenManageObject = NSManagedObject(entity: entity!, insertInto: context)
            tokenManageObject.setValue(InfoString[index], forKeyPath: key)
            
            // Now we need to "commit" the changes
            do {
                try context.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
            
        
        }

    } // End of saveTokenFromCoreDate method
    
    func  fetchFromCoreDataToArrayImages(entityName: String, key: String) -> [UIImage]{
        
        let data = fetchFromCoreData(entityName: entityName, key: key)
        
        var strings = [UIImage]()
        
        for index in 0...(data.count - 1){
            
            let datos = data[index].value(forKey: key)
            
                if (datos != nil){
                strings.append(UIImage(data: (datos as! NSData) as Data)!)
            }
            
        }
            //
            //strings.append(data[index].value(forKey: key) as! UIImage)
        
        
        return strings
    } // End of fetchFromCoreDataToArrayString
    
    
    
    func saveCoreBinaryData(image: Data, entityName: String, key: String){
        // Save the token in the coreData
        let context: NSManagedObjectContext = getContext()
        
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        
        let tokenManageObject = NSManagedObject(entity: entity!, insertInto: context)
            
        tokenManageObject.setValue(image, forKeyPath: key)
            
            // Now we need to "commit" the changes
            do {
                try context.save()
                
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
            
            
        
        
    } // End of saveTokenFromCoreDate method
    
    func  fetchFromCoreDataToArrayString(entityName: String, key: String) -> [String]{
        
        let data = fetchFromCoreData(entityName: entityName, key: key)
        
        var strings = [String]()
        
        for index in 0...(data.count - 1){
            let datos = data[index].value(forKey: key)
            
            if datos != nil{
            strings.append(datos as! String)    
            }
            
        }
        
        return strings
    } // End of fetchFromCoreDataToArrayString
    
    
    func fetchFromCoreData(entityName: String, key: String) -> [NSManagedObject]{
        // Fecth the core data to the caller.
        let context: NSManagedObjectContext = getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        var coreElements: [NSManagedObject] = [NSManagedObject]()
        
        do {
            
            coreElements = try context.fetch(fetchRequest)
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        if coreElements.last == nil{
            let entity = NSEntityDescription.entity(forEntityName: key, in: context)
            let tokenManageObject = NSManagedObject(entity: entity!, insertInto: context)
            tokenManageObject.setValue("", forKeyPath: key)
            return [tokenManageObject]
            
        }else {
            return coreElements
        }
        
        
    } // End of fecthFromCoreData function
    
    func deleteDataForEntity(entityName: String){
        let context: NSManagedObjectContext = getContext()
        
        
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)

        do {
            try context.execute(request)

            
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
        
    } // End of deleteDataForEntity
    
    

    
    func getContext() ->NSManagedObjectContext {
        // This function returns the context needed to save and retrieve persistance data.
        var context: NSManagedObjectContext?
        context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        return context!
    } // End of method getContext
    
    
}

enum MyError : Error {
    case RuntimeError(String)
}
