//
//  CategoriesVC.swift

//  Created by Hugo Reyes on 1/27/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//

import UIKit


class CategoriesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, Utilities {
    
    let endpoints = Endpoints()
    
    var json: NSDictionary?
    
    var categories: [String] = [String]()
    
    let coredata: CoreDataUtilities = CoreDataUtilities()
    
    var gradientLayer: CAGradientLayer = CAGradientLayer()
    
    let backGroundColors = [UIColor.orangeRappi, UIColor.orangeRappiClear, UIColor.greenRappiClear,
                            UIColor.yellowRappiClear, UIColor.redRappiClear]
    
    // Table view
    let tableViewObject: UITableView = {
        let table: UITableView = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    

    
override func viewDidLoad() {
    super.viewDidLoad()
    
    self.customNavigationBar()
    self.addSubviews()
    self.setupLayout()
    
    
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
    
    if (self.isInternetAvailable()){
        
        self.fetchJson()
        self.coredata.deleteDataForEntity(entityName: "Screen")
        self.coredata.saveNumberScreen(screenNumber: "1")
        
        
    }else{
        print("There is no internet connection")
        
        if (self.coredata.entityIsEmpty(entity: "Screen") == false){
        
            if(self.coredata.readNumberScreen() == "2"){
                let appResults = AppResultsVC()
                self.navigationController?.pushViewController(appResults, animated: false)
                return
        }
            
        }
        
        self.alertControllerMsg(viewController: self, title: "Warning", message: "There is no internet connection. Please verify Internet and restart the app.")
        
        if (self.coredata.entityIsEmpty(entity: "Categories") == false){

            self.categories = self.coredata.fetchFromCoreDataToArrayString(entityName: "Categories", key: "category")
        }
    
    }
    
   } // End of viewDidLoad
    
    
    
    
    override func viewDidLayoutSubviews() {
        gradientLayer.frame = self.view.bounds
    }

    
    func addSubviews(){
        self.view.addSubview(self.tableViewObject)
    }
    
    
    
    func setupLayout(){
        
        self.gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [ UIColor.createColorObject(r: 48, g: 182, b: 184).cgColor, UIColor.orangeRappi.cgColor]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)

        self.tableViewObject.backgroundColor = UIColor.clear
        self.tableViewObject.delegate = self
        self.tableViewObject.dataSource = self
        self.tableViewObject.register(CategoryCell.self, forCellReuseIdentifier: "cellId")
        
        self.tableViewObject.buildingAnchorConstraint(
            referenceView: self.view,
            topAnchorConstant: 40,
            bottomAnchorConstant: 0,
            widthAnchorMultiplier: 1,
            centerXAnchor: true)
        
    
        
        
    }
    
    ////////////////////////// Custom Navigation Bar ////////////////////////////
    func customNavigationBar(){

        self.navigationController?.navigationBar.topItem?.title = "App Top"
        
    } // End of custom navigation bar function
    
    

    func fetchJson(){
        // This methods fetch the JSON file from URL
        
        self.endpoints.requestHttps{
            jsonFeed,  entryList in Void()
            /*  DispatchQueue manages the execution of work items. Each work item submitted to a queue is processed on a pool of threads managed by the system.
             */
                DispatchQueue.main.async(){
                
                self.json = jsonFeed as NSDictionary
                    
                // Saving the categories
                self.categories = self.getCategories()
                self.coredata.deleteDataForEntity(entityName: "Categories")
                self.coredata.saveCoreDataInfo(InfoString: self.categories, entityName: "Categories", key: "category")

                self.tableViewObject.reloadData()
                }
    } // End of dispathQueueCallBack function
        
    }
    
    
    
    func getCategories() -> [String]{
        // This method search for categories and returns all different categories in the list
        
        let listAppsItem: [Any] = self.json?.value(forKey: "entry") as! [Any]

        var categories: [String] = [String]()
        
        // Getting the different categories:
        for index in 0...(listAppsItem.count - 1){
            let categoryDict = (listAppsItem[index] as! NSDictionary).value(forKey: "category") as! NSDictionary
            
            categories.append((categoryDict.value(forKey: "attributes") as! NSDictionary).value(forKey: "label") as! String)
            
        }
        
        return Array(Set(categories))
        
    } // End of getCategories
    
   
    func nextWindow(sender: UIButton){
        let appResults = AppResultsVC()

        if (self.isInternetAvailable()){
            self.coredata.deleteDataForEntity(entityName: "App")
            self.coredata.saveAppList(appsList: self.getAppsList(category: self.categories[sender.tag]))
            self.navigationController?.pushViewController(appResults, animated: true)
            
        }else{
            self.alertControllerMsg(viewController: self, title: "Warning", message: "There is no internet connection")
        }
    }
    
    
    
    func getAppsList(category: String) -> [AppClass]{
        // This function returns all apps of a certain category except the images data (only the urlImage)
        
        var appArray: [AppClass] = [AppClass]()
        
        let listAppsItem: [Any] = (self.json?.getListFromKey(key: "entry"))!
        var counter = 0
        
        // Getting the different categories:
        for index in 0...(listAppsItem.count - 1){
            
            let categoryDict = (listAppsItem[index] as! NSDictionary).getDictFromKey(key: "category")
            
            let summaryDict = (listAppsItem[index] as! NSDictionary).getDictFromKey(key: "summary")
            
            let currentCategory = ((categoryDict.value(forKey: "attributes") as! NSDictionary).value(forKey: "label") as! String)
            
            let appDescription = summaryDict.value(forKey: "label") as! String
            //print(description)
            
            if (currentCategory == category){
                
                let name = (listAppsItem[index] as! NSDictionary).getDictFromKey(key: "im:name").value(forKey: "label")! as! String
                
          
                
                let imageUrl = ((listAppsItem[index] as! NSDictionary).getListFromKey(key: "im:image")[Int(UIScreen.main.scale)] as! NSDictionary).value(forKey: "label")! as! String
                
                appArray.append(AppClass(title: name, urlImage: imageUrl, appDescription: appDescription))
                
                counter = counter + 1
                
            }
            
        }
        
        
        return appArray
    } // End of func getAppsList

    
    
    
    
    
    
    
    
    
    //
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
        
    } // End of heightForRowAt tableView function
    
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }
    

    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.categories.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! CategoryCell
     
            cell.setupViews()

            cell.categoryView.button.setTitle(self.categories[indexPath.row], for: .normal)

            // Setting cell colours
            cell.categoryView.button.backgroundColor = self.backGroundColors[indexPath.row % (self.backGroundColors.count - 1)]
        
            cell.categoryView.button.tag = indexPath.row
        
            cell.categoryView.setupLayout()
        
            cell.categoryView.button.addTarget(self, action: #selector(nextWindow), for: .touchDown)
        
        
            return cell
    } // End of func tableView
    
    
    

} // End of CheckinMainVC


