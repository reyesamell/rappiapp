//
//  DetailAppVC.swift
//  RappiApp
//
//  Created by iMac3 on 2/11/17.
//  Copyright © 2017 iMac3. All rights reserved.
//

import UIKit

class DetailAppVC: UIViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate {

    let titleLabel = LabelMedium(text: "", size: 18)
    

    
    let imageApp = CustomImageClass()
    
    //let descriptionLabel = LabelMedium(text: "", size: 12)
    
    let descriptionLabel: UITextView = {
        let text: UITextView = UITextView()
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    
    }()
    
    
    let cardView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addSubViews()
        //self.setupLayout()
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func addSubViews(){
        self.view.addSubview(self.cardView)
        self.cardView.addSubview(self.titleLabel)
        self.cardView.addSubview(self.imageApp)
        self.cardView.addSubview(self.descriptionLabel)
    
    }
    
    func closeWindow(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupLayout(){
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(closeWindow))
        
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        
        self.view.addGestureRecognizer(tapGesture)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.isOpaque = false
        
        self.titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.titleLabel.numberOfLines = 0
        
        self.view.layoutIfNeeded()
        
  
        self.cardView.buildingAnchorConstraint(referenceView: self.view, heightAnchorMultiplier: 0.8, widthAnchorMultiplier: 0.7, centerXAnchor: true, centerYAnchor: true)


        self.cardView.backgroundColor = .white
        self.cardView.layer.cornerRadius = 20
        self.cardView.layoutIfNeeded()
        
        self.cardView.layer.shadowColor = UIColor.gray.cgColor
        self.cardView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.cardView.layer.shadowOpacity = 1
        self.cardView.layer.shadowRadius = 5

        self.titleLabel.buildingAnchorConstraint(referenceView: self.cardView, topAnchorConstant: 5, widthAnchorMultiplier: 0.8, centerXAnchor: true, fixedHeight: 70)
        
        self.imageApp.buildingAnchorConstraint(referenceView: self.cardView, centerXAnchor: true, fixedHeight: 72, fixedWidth: 72)
        
        self.imageApp.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 5).isActive = true
        
        self.descriptionLabel.buildingAnchorConstraint(referenceView: self.cardView, bottomAnchorConstant: 0, widthAnchorMultiplier: 0.9, centerXAnchor: true)
        
        self.descriptionLabel.topAnchor.constraint(equalTo: self.imageApp.bottomAnchor, constant: 5).isActive = true
        self.descriptionLabel.isEditable = false
        
        
        
    }


}
