//
//  AppResultsVC.swift
//  RappiApp
//
//  Created by iMac3 on 2/9/17.
//  Copyright © 2017 iMac3. All rights reserved.
//

import UIKit

class AppResultsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, Utilities{
    
    // Data variables
    var categories: [String] = [String]()
    let endpoints = Endpoints()
    var appsList: [AppClass]?
    let coredata: CoreDataUtilities = CoreDataUtilities()
    var category: String?
    
    // Layout and interface variables
    var cardCollectionView : UICollectionView?
    var pointEstimator: RelativeLayoutUtilityClass?
    let mainLabel = LabelMedium(text: "", size: 20)

    
    // Variables asociated to collection view:
    fileprivate var currentPage: Int = 0
    
    fileprivate var pageSize: CGSize {
        let layout = self.cardCollectionView?.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        pageSize.width += layout.minimumLineSpacing
        return pageSize
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

            if (self.isInternetAvailable()){
                
                self.appsList = self.coredata.readAppList()

                self.requestData()
                self.coredata.deleteDataForEntity(entityName: "Screen")
                self.coredata.saveNumberScreen(screenNumber: "2")
                
            }else{ // There is no internet
        
                self.alertControllerMsg(viewController: self, title: "Warning", message: "There is no internet connection")
                
                self.appsList = self.coredata.readAppList()
   
              
            }
        

        
        
        self.pointEstimator = RelativeLayoutUtilityClass(referenceFrameSize: self.view.frame.size)
        self.addCardCollectionView()
        self.setupLayout()
        

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.customNavigationBar()
    }
 
    func requestData(){
        // Request images
        for index in 0...((self.appsList?.count)! - 1){
            
        self.endpoints.downloadImage(index: index, urlString: (self.appsList?[index].urlImage)!) { (indexCallback, image) in
             DispatchQueue.main.async(){
                
                    self.appsList![indexCallback].image = image!
                    self.cardCollectionView?.reloadData()
                
                    // Delete and save the new data
                    self.coredata.deleteDataForEntity(entityName: "App")
                    self.coredata.saveAppList(appsList: self.appsList!)
              
                
                } // End of dispatchQueue
            } // End of downloadImage
        } // End of for
    } // End of requestData
    
    
    
    func addCardCollectionView(){
        
        let layout = UPCarouselFlowLayout()
        layout.scrollDirection = .horizontal
    
        if UIDevice.current.orientation == UIDeviceOrientation.portrait {
            layout.itemSize = CGSize(width: (pointEstimator?.relativeWidth(multiplier: 0.5))!, height: (pointEstimator?.relativeHeight(multiplier: 0.3))!)
        }else{
            layout.itemSize = CGSize(width: (pointEstimator?.relativeWidth(multiplier: 0.3))!, height: (pointEstimator?.relativeHeight(multiplier: 0.5))!)
        }
        
        print("Pantallazo: ", UIScreen.main.scale)

        self.cardCollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        self.cardCollectionView?.translatesAutoresizingMaskIntoConstraints = false
        self.cardCollectionView?.delegate = self
        self.cardCollectionView?.dataSource = self
        self.cardCollectionView?.register(AppCardCell.self, forCellWithReuseIdentifier: "cellId")
        self.cardCollectionView?.backgroundColor = .clear
        
        // Setting collectionView layout
        let spacingLayout = self.cardCollectionView?.collectionViewLayout as! UPCarouselFlowLayout
        spacingLayout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 20)
        spacingLayout.sideItemAlpha = 0.9
        spacingLayout.sideItemScale = 0.9
        
        self.view.addSubview(self.cardCollectionView!)
    }

    
    func setupLayout(){
        
        self.view.createGradientLayer(colors: [UIColor.orangeRappi.cgColor, UIColor.orangeRappiClear.cgColor])
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.cardCollectionView?.buildingAnchorConstraint(
            referenceView: self.view,
            widthAnchorMultiplier: 1,
            centerXAnchor: true,
            centerYAnchor: true,
            fixedHeight: (pointEstimator?.relativeHeight(multiplier: 0.8))!)
        
        self.currentPage = 0
        self.cardCollectionView?.showsHorizontalScrollIndicator = false
        

    } // End of setupLayout function
    
    ////////////////////////// Custom Navigation Bar ////////////////////////////
    func customNavigationBar(){
        
        self.tabBarController?.navigationItem.title =  "App list"

        
        
    } // End of custom navigation bar function
    
    
    
    

    
    func backToLastWindow(){
        self.coredata.deleteDataForEntity(entityName: "Screen")
        self.coredata.saveNumberScreen(screenNumber: "1")
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func nextWindow(sender: UIButton){
        let detailApp = DetailAppVC()
        
        detailApp.titleLabel.text = self.appsList![sender.tag].title
        detailApp.imageApp.image = self.appsList![sender.tag].image
        detailApp.descriptionLabel.text = self.appsList![sender.tag].appDescription
        detailApp.modalPresentationStyle = .overCurrentContext
        detailApp.modalTransitionStyle = .crossDissolve
        
        detailApp.setupLayout()
        
        self.present(detailApp, animated: true, completion: nil)
        
    }
    
    
    // MARK: - Card Collection Delegate & DataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.appsList!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! AppCardCell
        
        cell.imageApplication.image = self.appsList?[indexPath.row].image
        cell.appLabel.text = self.appsList?[indexPath.row].title
        cell.button.addTarget(self, action: #selector(nextWindow), for: .touchDown)
        cell.button.tag = indexPath.row
        cell.setupLayout()
        return cell
            

        
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
            let layout = self.cardCollectionView?.collectionViewLayout as! UPCarouselFlowLayout
            let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
            let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
            currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
            

        
    }
    
    

    
}




