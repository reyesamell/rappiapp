//
//  Label11pt.swift

//  Created by Hugo Reyes on 1/31/17.
//  Copyright © 2017 Hugo Reyes. All rights reserved.
//

import UIKit

class LabelMedium: UILabel {
    // LogoCategoryc class
    
    init(text: String, size: CGFloat){
        // This line is needed to run the super class constructor.
        // The frame is set to zero, because the size of the class
        // will be changed in the future
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        
        self.font = UIFont.systemFont(ofSize: size)
        //self.font = UIFont(name: "Now-medium", size: size)
        self.textColor = UIColor.createColorObject(r: 59, g: 55, b: 75)
        self.text = text
        self.textAlignment = .center
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
