//
//  Utilities.swift

//
//  Created by Hugo Reyes on 11/25/16.
//  Copyright © 2016 Hugo Reyes. All rights reserved.
//
import UIKit

protocol Utilities {
    func alertControllerMsg(viewController: UIViewController, title: String, message: String)
    
}


extension Utilities {
    func alertControllerMsg(viewController: UIViewController, title: String, message: String){
        /*
         This function is a alertControllerMsg builder helper.
         Params:
         viewController, is tipically self.
         title, is the title mesage controller
         message, is the message content to show in controller.
         
         */
        
        // AlertController instanctiation
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        // Cancel action for AlertController
        let cancelAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: {(action) in
        })
        
        // Adding the action to the alertController
        alertController.addAction(cancelAction)
        
        // Setting the controller as the present controller
        viewController.present(alertController, animated: true, completion: nil)
    } // End of alertControllerMsg
    
    
}

